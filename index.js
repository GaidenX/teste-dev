var fs = require("fs");
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var porta = process.env.PORT || 8080;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(express.static(__dirname + '/public'));

// Data SQL
var mysql = require('mysql');

var conSql = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "registers"
});

conSql.connect(function (err) {
    if (err) console.log('Sql não conectado'); //throw err
    else {
        console.log("Sql Conectado!");
        var sql = "select * from clients";
        conSql.query(sql, function (err, result) {
            if (err) throw err;
        });
    }
});

// ---

//Paginas - Rotas
app.get('/home', function (request, response, next) {
    response.sendfile('public/index.html');
});

app.get('/cliente', function (request, response, next) {
    response.sendfile('public/form_client.html');
});

app.get('/produto', function (request, response, next) {
    response.sendfile('public/form_product.html');
});

app.get('/vendas', function (request, response, next) {
    response.sendfile('public/sales.html');
});

app.get('/licensa', function (request, response, next) {
    response.sendfile('public/license.html');
});

//---

/* Post on SQL */
// Client
app.post('/cadastroCliente', function (request, response) {
    var sql = "insert into clients(name,address,email) values ?";

    var values = [
        [request.body.clientName, request.body.clientAddress, request.body.clientEmail]
    ];

    conSql.query(sql, [values], function (err, result, fields) {
        if (err) response.status(400).send('reprovado');
        else
            response.status(200).send('aprovado');
    });
});

app.post('/removerCliente', function (request, response) {
    var sql = "delete from clients where name='" + request.body.clientName + "'";
    console.log('Removendo cliente: ' + request.body.clientName);

    conSql.query(sql, function (err, result, fields) {
        if (err) response.status(400).send('reprovado');
        else
            response.status(200).send('aprovado');
    });
});

// Product
app.post('/cadastroProduto', function (request, response) {
    var sql = "insert into products(name,description) values ?";

    var values = [
        [request.body.productName, request.body.productDesc]
    ];

    conSql.query(sql, [values], function (err, result, fields) {
        if (err) response.status(400).send('reprovado');
        else
            response.status(200).send('aprovado');
    });
});

app.post('/removerProduto', function (request, response) {
    var sql = "delete from products where name='" + request.body.productName + "'";
    console.log('Removendo produto: ' + request.body.productName);

    conSql.query(sql, function (err, result, fields) {
        if (err) response.status(400).send('reprovado');
        else
            response.status(200).send('aprovado');
    });
});

// Route Search
app.get('/buscarProdutos', function (request, response) {
    var sql = "select * from products";

    var searchResult = {
        "id": [],
        "name": []
    };

    conSql.query(sql, function (err, result) {
        for (var index in result) {
            searchResult.id.push(result[index].id);
            searchResult.name.push(result[index].name)
        }
        if (err) response.status(400).send('error');
        else response.status(200).send(searchResult);
    });
});

app.post('/buscarCliente', function (request, response) {
    var sql = "select * from clients where name like '%" + request.body.searchTerm + "%'";

    var searchResult = {
        "id": [],
        "name": []
    };

    conSql.query(sql, function (err, result, fields) {
        for (var index in result) {
            searchResult.id.push(result[index].id);
            searchResult.name.push(result[index].name)
        }
        if (err) response.status(400).send('error');
        else
            response.status(200).send(searchResult);
    });
});

app.listen(porta, function () {
    console.log("server on in: " + porta)
});