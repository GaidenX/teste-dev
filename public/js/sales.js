// Store Products List
$(function () {
    console.log('buscar produto')
    $.ajax({
        traditional: true,
        type: "GET",
        url: "/buscarProdutos",
        success: function (data) {
            for (var key in data.id) {
                $('.listProducts').append('<option value="' + data.id[key] + '">' + data.id[key] + ' - ' + data.name[key] + '</option>');
            }
        },
        error: function (data) {
            popupS.alert({
                content: 'Ocorreu um problema na busca de produtos, reinicie a página!'
            });
        }
    });
});

// Search Client
$("#searchClient").on('submit', function (event) {
    event.preventDefault();
    that = jQuery(this).attr("id");

    var data = {
        "searchTerm": $('#' + that + ' #search_client').val()
    }

    // Limpar o campo
    $('#' + that + ' #search_client').val('');

    $.ajax({
        traditional: true,
        type: "POST",
        url: "/buscarCliente",
        data: data,
        success: function (data) {
            $('.listClient').empty();

            for (var key in data.id) {
                $('.listClient').append('<option value="' + data.id[key] + '">' + data.id[key] + ' - ' + data.name[key] + '</option>');
            }
        },
        error: function (data) {
            popupS.alert({
                content: 'Ocorreu um problema na busca, tente novamente!'
            });
        }
    });
});

function salesRegister() {
    if ($('.salesTemplate.activeSales').attr('data-id') == 'clients') {
        $('.salesTemplate[data-id="clients"]').removeClass('activeSales').addClass('inactiveSales');
        $('.salesTemplate[data-id="products"]').removeClass('activeSales').addClass('activeSales');
    }
}


// $('select').on('change', function() {
//     alert( this.value );
//   });

// $('.form-control option:selected');