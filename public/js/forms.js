// Client
$("#registerClient").on('submit', function (event) {
    event.preventDefault();
    that = jQuery(this).attr("id");

    var data = {
        "clientName": $('#' + that + ' #client_name').val(),
        "clientEmail": $('#' + that + ' #client_email').val(),
        "clientAddress": $('#' + that + ' #client_address').val()
    }

    $.ajax({
        traditional: true,
        type: "POST",
        url: "/cadastroCliente",
        data: data,
        success: function (data) {
            popupS.alert({
                content: 'Cliente cadastrado com sucesso!'
            });
        },
        error: function (data) {
            popupS.alert({
                content: 'Problemas no cadastro do cliente, tente novamente!'
            });
        }
    });
});

$("#removeClient").on('submit', function (event) {
    event.preventDefault();
    that = jQuery(this).attr("id");

    var data = {
        "clientName": $('#' + that + ' #client_name').val()
    }

    $.ajax({
        traditional: true,
        type: "POST",
        url: "/removerCliente",
        data: data,
        success: function (data) {
            popupS.alert({
                content: 'Cliente removido com sucesso!'
            });
        },
        error: function (data) {
            popupS.alert({
                content: 'Cliente não pode ser removido ou encontrado, tente novamente!'
            });
        }
    });
});


// Product
$("#registerProduct").on('submit', function (event) {
    event.preventDefault();
    that = jQuery(this).attr("id");

    var data = {
        "productName": $('#' + that + ' #product_name').val(),
        "productDesc": $('#' + that + ' #product_desc').val()
    }

    $.ajax({
        traditional: true,
        type: "POST",
        url: "/cadastroProduto",
        data: data,
        success: function (data) {
            popupS.alert({
                content: 'Produto cadastrado com sucesso!'
            });
        },
        error: function (data) {
            popupS.alert({
                content: 'Problemas no cadastro do produto, tente novamente!'
            });
        }
    });
});

$("#removeProduct").on('submit', function (event) {
    event.preventDefault();
    that = jQuery(this).attr("id");

    var data = {
        "productName": $('#' + that + ' #product_name').val()
    }

    $.ajax({
        traditional: true,
        type: "POST",
        url: "/removerProduto",
        data: data,
        success: function (data) {
            popupS.alert({
                content: 'Produto removido com sucesso!'
            });
        },
        error: function (data) {
            popupS.alert({
                content: 'O produto não pode ser removido ou encontrado, tente novamente!'
            });
        }
    });
});