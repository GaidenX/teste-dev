# Requisitos
- Npm
- [ Nodejs ](https://nodejs.org/en/download/)
- MySQL
- WampServer com MySQL Workbench (Recomendado)

# Como Rodar

Primeiramente importar o banco de dados SQL, pode importar o arquivo sql ou então linkar com algum outro com a estrutura igual a enviada.
Caso queira utilizar outro banco basta ir no arquivo index.js na raiz e substituir os valores:

var conSql = mysql.createConnection({
    host: "", // o host do seu banco sql / localhost por exemplo
    user: "root", // o usuário do seu banco / login
    password: "", // o senha do seu banco se houver
    database: "registers" // Nome do banco de dados que contem as tabelas
});

No terminal, acesse a pasta do projeto e digite:
$ npm install (espere finalizar de instalar as dependencias)
$ node index.js (por default esta em http://localhost:8080)

Apos isso no seu terminal tera uma mensagem informando se a coneção com o banco foi concluida com exito e a aplicação estará rodando.